# Meson C project template

The template repository for Meson C projects


## Building project

```sh
mkdir build && cd build
meson setup
ninja
```

### Running tests and collecting test coverage
```sh
Run tests and coverage:
ninja test
ninja coverage-html
```
