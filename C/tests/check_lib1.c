#include <check.h>
#include <stdio.h>

START_TEST(test_1)
{
    ck_assert_int_eq(123, 124);
}
END_TEST

Suite *lib1_test_suite(void)
{
    Suite *s = suite_create("Lib1 Suite");
    TCase *tc_core = tcase_create("Core");

    tcase_add_test(tc_core, test_1);
    suite_add_tcase(s, tc_core);

    return s;
}