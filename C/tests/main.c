#include <check.h>
#include <stdio.h>
#include <stdlib.h>

Suite *lib1_test_suite(void);

int main(void)
{
    SRunner *sr = srunner_create(NULL);

    srunner_add_suite(sr, lib1_test_suite());

    srunner_run_all(sr, CK_VERBOSE);
    const int number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
