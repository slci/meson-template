project('meson_c_template, 'c',
    version: '0.1',
    license: 'MIT',
    default_options : [
		'c_std=c11',
		'warning_level=3',
		'werror=true',
		'buildtype=debugoptimized',
		'pkg_config_path=.',
		'b_coverage=true',
		'b_lto=true',
		'b_pie=true'
    ]
)

# additional compilation arguments
add_global_arguments(
	'-Wshadow',
	language : 'c'
)

# install dependencies
conan = find_program('conan', required : true)
pkg_config = find_program('pkg-config', required : true)

doxygen = find_program('doxygen', required : true)
if doxygen.found()
	message('Doxygen found')
	run_target('doxygen', command : [doxygen, meson.source_root() + '/Doxyfile'])
else
	error('Doxygen not found')
endif

conan_out = run_command('conan', 'install', '--build=missing', '-if', meson.build_root(), meson.source_root(), check: true)
if conan_out.returncode() != 0
	error(conan_out.stderr())
else
	message(conan_out.stdout())
endif

inc_dirs = include_directories('include')

subdir('src')
subdir('tests')

pkg_mod = import('pkgconfig')
pkg_mod.generate(libraries : [library1],
                 name : meson.project_name(),
                 version : meson.project_version(),
                 filebase : meson.project_name(),
                 description : 'The template repository for C++ Meson projects')
