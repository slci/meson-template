# Meson C++ project template

[![Pipeline Status](https://gitlab.com/slci/meson-template/badges/master/pipeline.svg)](https://gitlab.com/slci/meson-template/-/commits/master)

The template repository for Meson C++ projects


## Building project

```sh
mkdir build && cd build
meson setup
ninja
```

### Running tests and collecting test coverage
```sh
Run tests and coverage:
ninja test
ninja coverage-html
```
